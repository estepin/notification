<?php
namespace Geekstart\Notification;

use Exception;
use Geekstart\Notification\contracts\Event;
use Geekstart\Notification\contracts\Handler;

class Service
{
    protected $handlers;
    protected $eventRepository;

    function __construct(contracts\EventRepository $repository)
    {
        $this->eventRepository = $repository;
    }

    /**
     * Регистрация события
     * @param $className
     */
    function registerEvent($className)
    {
        $this->eventRepository->registerEvent($className);
    }

    /**
     * Добавление обработчика событий
     * @param Handler $handler
     */
    function addHandler(Handler $handler)
    {
        $handler->setModule($this);
        $this->handlers[] = $handler;
    }

    /*
     * Добавляет событие
     */
    function addEvent($event)
    {
        $this->eventRepository->saveEvent($event);
        $this->runHandlers();
    }

    /*
     * Запускает обработчики
     */
    function runHandlers()
    {
        foreach ($this->handlers as $handler) {
            $handler->run();
        }
    }


    /**
     * Получает события.
     * @param EventRepoQuery $query
     * @return Event[]
     */
    function getEvents(EventRepoQuery $query)
    {
        return $this->eventRepository->getEvents($query);
    }

    /**
     * Устанавливает статус событию в связке с обработчиком
     * @param $eventId
     * @param $handlerName
     * @param $status
     */
    function setHandlerStatus($eventId, $handlerName, $status)
    {
        $this->eventRepository->setHandlerStatus($eventId, $handlerName, $status);
    }

    /**
     * Получает статус события в связке с обработчиком
     * @param $eventId
     * @param $handlerName
     * @return mixed|string
     * @throws Exception
     */
    function getHandlerStatus($eventId, $handlerName)
    {
        return $this->eventRepository->getHandlerStatus($eventId, $handlerName);
    }
}