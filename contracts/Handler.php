<?php
namespace Geekstart\Notification\contracts;


interface Handler
{
    const STATUS_NOT_HANDLED = 'not_handled';
    const STATUS_NOTIFIED = 'notified';

    function run();

    static function getName();

    function setModule($module);
}