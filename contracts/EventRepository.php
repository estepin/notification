<?php
namespace Geekstart\Notification\contracts;


use Geekstart\Notification\EventRepoQuery;

interface EventRepository
{
    function getEvents(EventRepoQuery $condition);

    function saveEvent(Event $event);

    function registerEvent($className);

    function getHandlerStatus($eventId, $handlerName);

    function setHandlerStatus($eventId, $handlerName, $status);
}