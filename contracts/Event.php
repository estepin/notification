<?php
namespace Geekstart\Notification\contracts;

use DateTime;

interface Event
{
    function getId();
    function setId(int $id);
    static function getType();
    function getCreatedDate() : DateTime;
    function setCreatedDate(DateTime $createdDate);

    function toArray() : array;
    static function initFromArray(array $array) : self;

}