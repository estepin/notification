Система оповещений
=======================

Эта система оповещений основана на событиях. 
Принцип такой:
Через функцию addEvent добавляется событие, оно сохраняется в Storage (бд или еще куда, не важно).

Далее запускаются обработчики. Обработчик может запросить нужные эвенты. 
По полученным эвентам можно сформировать письмо. Здесь в хендлере и находится логика формирования оповещения.

Например:
Хендлер запрашивает события типа NewMessages и получает одно событие. В этот момент отправляет письмо получателю о новом сообшении.
Или хендлер получает 10 событий и в таком случае формирует письмо о 10 сообщениях в одном письме.

Сценарий может быть любой. Главная особенность что можно пакетно обрабатывать события

Пример события
----
Событие обязательно должно реализовывать контракт contracts/Event. Или можно наследоваться от абстрактного
класса BaseEvent И реализовать методы toArray и initFromArray они нужны для конвертации
события в массив и обратно

```php
class NewMessage extends BaseEvent
{
    function __construct(Message $message)
    {
        $this->message = $message;
    }

    function toArray(): array
    {
        $arr = parent::toArray();
        $arr['message_id'] = $this->message->id;

        return $arr;
    }


    static function initFromArray(array $array): Event
    {
        $message = Message::findOne(['id' => $array['message_id']]);

        $obj = new self($message);
        $obj->id = $array['id'];
        $obj->createdDate = $array['createdDate'];

        return $obj;
    }
}
```



Пример обработчика
-----------
Обработчик запрашивает события типа NewMessages и затем обходит каждое событие проверяее статус что оно еще 
не обработано и если обработано отправляет письмо. Обработчик так же должен сам выставить статус что событие
было обработано  

```php
class NewMessagesHandlerEmail extends BaseHandler
{
    function run()
    {
        $query = (new EventRepoQuery())->byType(NewMessages::getType());
        $this->module->getEvents($query);  // Запрашиваем события NewMessages
        foreach ($events as $event) {
        
            $eventStatus = $this->module->getHandlerStatus($event->getId(), self::getName());
            if($eventStatus === self::STATUS_NOT_HANDLED) {
                //Если событие необработано, обрабатываем его здесь. Например, отправляем письмо о новом сообщении
                
                //Устанавливаем статус что событие обработано
                $this->module->setHandlerStatus($event->getId(), self::getName(), self::STATUS_NOTIFIED);
            }
            
        }
    }
}
```

Клиентский код
-----
Конфигурирование. Добавляем обработчик и регистрируем событие
```php
//$message - в данном случае модель сообщения

$notificationService = new Service();
$notificationService->addHandler(new NewMessagesHandlerEmail());
$notificationService->registerEvent(NewMessage::class);
```

Так добавляем событие когда пришло новое сообщение
```php
$notificationService->addEvent(new NewMessage($message));
```

