CREATE TABLE `notification_events` (
   `id` int NOT NULL AUTO_INCREMENT,
   `type` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
   `data` text COLLATE utf8mb4_general_ci,
   `updated_at` datetime NOT NULL,
   `created_at` datetime NOT NULL,
   PRIMARY KEY (`id`)
);

CREATE TABLE `notification_events_handlers` (
    `id` int NOT NULL AUTO_INCREMENT,
    `event_id` int NOT NULL,
    `handler_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
    `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
    `updated_at` datetime NOT NULL,
    `created_at` datetime NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `index2` (`event_id`,`handler_name`)
);


