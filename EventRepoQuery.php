<?php


namespace Geekstart\Notification;


class EventRepoQuery
{
    protected $type;
    protected $handlerStatus;

    function byType(string $type)
    {
        $this->type = $type;
        return $this;
    }

    function byHandlerStatus($handlerStatus)
    {
        $this->handlerStatus = $handlerStatus;
        return $this;
    }

    function toArray()
    {
        return [
            'type' => $this->type,
            'handlerStatus' => $this->handlerStatus
        ];
    }
}