<?php


namespace Geekstart\Notification\example;


class MessageModel
{
    public $id;
    public $text;

    static function create($arrData)
    {
        $obj = new self();
        $obj->id = $arrData['id'];
        $obj->text = $arrData['text'];

        return $obj;
    }

    static function getById($id)
    {
        return self::create(['id' => $id, 'text' => 'Test message']);
    }
}