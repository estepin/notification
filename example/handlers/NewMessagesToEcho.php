<?php


namespace Geekstart\Notification\example\handlers;


use Geekstart\Notification\contracts\Event;
use Geekstart\Notification\example\events\NewMessage;

class NewMessagesToEcho extends \Geekstart\Notification\handlers\BaseHandlerEach
{

    protected function typeEvent(): string
    {
        return NewMessage::getType();
    }

    /**
     * @param NewMessage $event
     */
    function runEach(Event $event)
    {
        $message = $event->message;
        echo sprintf("Добавлено новое сообщение. id: %s text: %s".PHP_EOL, $message->id, $message->text);
    }
}