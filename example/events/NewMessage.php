<?php
namespace Geekstart\Notification\example\events;


use Geekstart\Notification\contracts\Event;
use Geekstart\Notification\example\MessageModel;

class NewMessage extends \Geekstart\Notification\events\BaseEvent
{
    public $message;

    public function __construct($message)
    {
        $this->message = $message;
    }

    function toArray(): array
    {
        $arr = parent::toArray();
        $arr['message_id'] = $this->message->id;
        return $arr;
    }

    static function initFromArray(array $array): Event
    {
        $message = MessageModel::getById($array['message_id']);

        $obj = new self($message);
        $obj->id = $array['id'];
        $obj->createdDate = $array['createdDate'];

        return $obj;
    }
}