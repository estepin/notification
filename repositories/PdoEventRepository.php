<?php


namespace Geekstart\Notification\repositories;


use Exception;
use Geekstart\Notification\contracts\Event;
use Geekstart\Notification\contracts\Handler;
use Geekstart\Notification\EventRepoQuery;
use Geekstart\Notification\exceptions\RepositorySaveEvent;
use Geekstart\Notification\exceptions\UnknownTypeEvent;
use PDO;
use PDOException;

class PdoEventRepository implements \Geekstart\Notification\contracts\EventRepository
{
    protected $pdo;
    protected $table = 'notification_events';
    protected $handlerStatusTable = 'notification_events_handlers';
    protected $eventsClass;

    function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    function registerEvent($className)
    {
        $this->eventsClass[] = $className;
    }

    function getEvents(EventRepoQuery $condition)
    {
        $conditionArr = $condition->toArray();
        if(isset($conditionArr['handlerStatus']) && $conditionArr['handlerStatus']) {
            if(isset($conditionArr['type']) && $conditionArr['type']) {

                if($conditionArr['handlerStatus'] !== Handler::STATUS_NOT_HANDLED) {
                    $query = $this->pdo->prepare(
                        'SELECT events.* FROM ' . $this->table . ' AS events ' .
                        ' LEFT JOIN ' . $this->handlerStatusTable . ' AS hs ON hs.event_id = events.id' .
                        ' WHERE events.type=:type AND hs.status=:status'
                    );

                    $query->bindValue(':type', $conditionArr['type']);
                    $query->bindValue(':status', $conditionArr['handlerStatus']);
                } else {
                    $query = $this->pdo->prepare(
                        'SELECT events.* FROM ' . $this->table . ' AS events ' .
                        ' LEFT JOIN ' . $this->handlerStatusTable . ' AS hs ON hs.event_id = events.id' .
                        ' WHERE events.type=:type AND hs.id IS NULL'
                    );

                    $query->bindValue(':type', $conditionArr['type']);
                }

            } else {
                $query = $this->pdo->prepare(
                    'SELECT events.* FROM ' . $this->table . ' as events ' .
                    ' LEFT JOIN ' . $this->handlerStatusTable . 'hs ON hs.event_id = events.id' .
                    ' WHERE hs.status=:status'
                );

                $query->bindValue(':status', $conditionArr['handlerStatus']);
            }

        } else {
            $query = $this->pdo->prepare('SELECT * FROM '.$this->table.' WHERE type=:type');
            if(isset($conditionArr['type'])) {
                $query->bindValue(':type', $conditionArr['type']);
            }
        }


        $query->execute();

        $data = $query->fetchAll(PDO::FETCH_ASSOC);
        $result = [];
        foreach ($data as $item) {
            $result[] = $this->dbArrayToModel($item);
        }

        return $result;
    }

    function saveEvent(Event $event)
    {
        $arr = $this->modelToDbArray($event);

        if(!$event->getId()) {
            $arr['created_at'] = date('Y-m-d H:i:s');
            $arr['updated_at'] = date('Y-m-d H:i:s');



            $query = $this->pdo->prepare(
                'INSERT INTO '.$this->table.' (type, data, updated_at, created_at) VALUES (:type, :data, :updated_at, :created_at)'
            );
            $query->bindValue(':type', $arr['type']);
            $query->bindValue(':data', $arr['data']);
            $query->bindValue(':updated_at', $arr['updated_at']);
            $query->bindValue(':created_at', $arr['created_at']);

            try {
                $query->execute();
            } catch (PDOException $exception) {
                throw new RepositorySaveEvent($exception->getMessage());
            }

            $event->setId($this->pdo->lastInsertId());


        } else {
            $arr['updated_at'] = date('Y-m-d H:i:s');

            $query = $this->pdo->prepare(
                'UPDATE '.$this->table.' SET type=:type, data=:data, updated_at=:updated_at WHERE id=:id'
            );

            $query->bindValue(':type', $arr['type']);
            $query->bindValue(':data', $arr['data']);
            $query->bindValue(':updated_at', $arr['updated_at']);
            $query->bindValue(':id', $event->getId());

            try {
                $query->execute();
            } catch (PDOException $exception) {
                throw new RepositorySaveEvent($exception->getMessage());
            }
        }
    }

    protected function modelToDbArray(Event $event) : array
    {
        $array = $event->toArray();
        unset($array['id']);
        unset($array['type']);
        unset($array['createdDate']);

        $dbArray = [
            'type' => $event::getType(),
            'data' => json_encode($array)
        ];

        if($event->getId()) {
            $dbArray['id'] = $event->getId();
        }

        return $dbArray;
    }

    protected function dbArrayToModel($arr) : Event
    {
        $classEvent = $this->classByType($arr['type']);
        $dataArr = array_merge($arr, json_decode($arr['data'], true));

        $dataArr['createdDate'] = $dataArr['created_at'];
        unset($dataArr['data']);
        unset($dataArr['created_at']);

        return $classEvent::initFromArray($dataArr);
    }

    protected function classByType(string $type) : string
    {
        foreach ($this->eventsClass as $className)
        {
            if($className::getType() == $type) {
                return $className;
            }
        }

        throw new UnknownTypeEvent();
    }

    function getHandlerStatus($eventId, $handlerName)
    {
        $data = $this->getHandlerStatusData($eventId, $handlerName);

        if($data) {
            if(!isset($data['status'])) {
                throw new Exception('Not valid data');
            }
            $dbStatus = $data['status'];

            $statuses = [
                'notified' => Handler::STATUS_NOTIFIED
            ];

            if(isset($statuses[$dbStatus])) {
                return $statuses[$dbStatus];
            } else {
                throw new Exception('Unknown status '.$dbStatus);
            }
        } else {
            return Handler::STATUS_NOT_HANDLED;
        }
    }

    function setHandlerStatus($eventId, $handlerName, $status)
    {
        $data = $this->getHandlerStatusData($eventId, $handlerName);

        if($data) {
            $this->updateHandlerStatus($data['id'], $status);
        } else {
            $this->insertHandlerStatus($eventId, $handlerName, $status);
        }
    }

    protected function insertHandlerStatus($eventId, $handlerName, $status)
    {
        $query = $this->pdo->prepare(
            'INSERT INTO '.$this->handlerStatusTable.' (event_id, handler_name, status, updated_at, created_at) VALUES (:eventId, :handlerName, :status, :updatedAt, :createdAt)'
        );

        $query->bindValue(':eventId', $eventId);
        $query->bindValue(':handlerName', $handlerName);
        $query->bindValue(':status', $status);
        $query->bindValue(':updatedAt', date('Y-m-d H:i:s'));
        $query->bindValue(':createdAt', date('Y-m-d H:i:s'));

        $query->execute();
    }

    protected function updateHandlerStatus($id, $status)
    {
        $query = $this->pdo->prepare(
            'UPDATE '.$this->handlerStatusTable.' SET status=:status WHERE id=:id'
        );

        $query->bindValue('id', $id);
        $query->bindValue('status', $status);

        $query->execute();
    }

    protected function getHandlerStatusData($eventId, $handlerName)
    {
        $query = $this->pdo->prepare(
            'SELECT * FROM '.$this->handlerStatusTable.' WHERE event_id=:eventId AND handler_name=:handlerName'
        );
        $query->bindValue('eventId', $eventId);
        $query->bindValue('handlerName', $handlerName);
        $query->execute();

        return $query->fetch();
    }
}