<?php
namespace Geekstart\Notification\events;


use Geekstart\Notification\contracts\Event;
use \DateTime;
use \Exception;
use ReflectionClass;

abstract class BaseEvent implements Event
{
    protected $id;
    protected $createdDate;

    function getId()
    {
        return $this->id;
    }

    function setId(int $id)
    {
        $this->id = $id;
    }

    function getCreatedDate() : DateTime
    {
        return $this->createdDate;
    }

    function setCreatedDate(DateTime $createdDate)
    {
        if(!$this->createdDate) {
            $this->createdDate = $createdDate;
        } else {
            throw new Exception('Created date cannot be change');
        }
    }

    static function getType() : string
    {
        $reflection = new ReflectionClass(get_called_class());
        return $reflection->getShortName();
    }


    function toArray() : array
    {
        $arr = [
            'createdDate' => $this->createdDate,
            'type' => self::getType()
        ];

        if($this->getId()) {
            $arr['id'] = $this->id;
        }

        return $arr;
    }


    abstract static function initFromArray(array $array) : Event;
}