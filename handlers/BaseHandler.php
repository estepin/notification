<?php
namespace Geekstart\Notification\handlers;

use Geekstart\Notification\contracts\Handler;
use ReflectionClass;

abstract class BaseHandler implements Handler
{
    protected $module;

    function setModule($module)
    {
        $this->module = $module;
    }

    abstract function run();

    static function getName() : string
    {
        $reflection = new ReflectionClass(get_called_class());
        return $reflection->getShortName();
    }
}