<?php


namespace Geekstart\Notification\handlers;


use Geekstart\Notification\EventRepoQuery;
use Geekstart\Notification\contracts\Event;

abstract class BaseHandlerEach extends BaseHandler
{
    function run()
    {
        $query = (new EventRepoQuery())->byType($this->typeEvent())->byHandlerStatus(self::STATUS_NOT_HANDLED);
        $events = $this->module->getEvents($query);
        foreach ($events as $event) {
            if($this->module->getHandlerStatus($event->getId(), self::getName()) === self::STATUS_NOT_HANDLED) {

                $this->runEach($event);

                $this->module->setHandlerStatus($event->getId(), self::getName(), self::STATUS_NOTIFIED);
            }
        }
    }

    abstract protected function typeEvent() : string;

    abstract function runEach(Event $event);
}